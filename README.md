# UniprotGeneOntology

This project demonstrates how to retrieve GeneOntology annotations from [UniProt SPARQL endpoint](https://sparql.uniprot.org/), and combine them with the [hierarchy from the GeneOntology](https://geneontology.org/).

